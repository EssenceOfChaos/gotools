# slower version
defmodule Equilibrium do
  def index(list) do
    last = length(list)

    Enum.filter(0..(last - 1), fn i ->
      Enum.sum(Enum.slice(list, 0, i)) == Enum.sum(Enum.slice(list, (i + 1)..last))
    end)
  end
end

# faster version
defmodule Equilibrium do
  def index(list), do: index(list, 0, 0, Enum.sum(list), [])

  defp index([], _, _, _, acc), do: Enum.reverse(acc)

  defp index([h | t], i, left, right, acc) when left == right - h,
    do: index(t, i + 1, left + h, right - h, [i | acc])

  defp index([h | t], i, left, right, acc), do: index(t, i + 1, left + h, right - h, acc)
end

# indices = [
#   [-7, 1, 5, 2,-4, 3, 0],
#   [2, 4, 6],
#   [2, 9, 2],
#   [1,-1, 1,-1, 1,-1, 1]
# ]
# Enum.each(indices, fn list ->
#   IO.puts "#{inspect list} => #{inspect Equilibrium.index(list)}"
# end)
